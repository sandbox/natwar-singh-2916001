<?php

/**
 * @file
 * SNS OTP configuration file.
 */

/**
 * AWS configuration form.
 *
 * @see sns_otp_login_menu().
 */
function sns_otp_aws_credentials($form, &$form_state) {
  $form['sns_otp_aws_key'] = array(
    '#type' => 'textfield',
    '#title' => t('Key'),
    '#default_value' => variable_get('sns_otp_aws_key', ''),
    '#description' => t("AWS Key."),
    '#required' => TRUE,
  );
  $form['sns_otp_aws_secret'] = array(
    '#type' => 'textfield',
    '#title' => t('Secret'),
    '#default_value' => variable_get('sns_otp_aws_secret', ''),
    '#description' => t("AWS Secret."),
    '#required' => TRUE,
  );
  $form['sns_otp_aws_region'] = array(
    '#type' => 'select',
    '#title' => t('Region'),
    '#options' => array(
      'us-east-1' => t('US East (N. Virginia)'),
      'us-west-2' => t('US West (Oregon)'),
      'eu-west-1' => t('EU (Ireland)'),
      'ap-southeast-1' => t('Asia Pacific (Singapore)'),
      'ap-southeast-2' => t('Asia Pacific (Sydney)'),
      'ap-northeast-1' => t('Asia Pacific (Tokyo)'),
     ),
    '#multiple' => FALSE,
    '#default_value' => variable_get('sns_otp_aws_region', ''),
    '#description' => t('Select a AWS region.'),
    '#required' => TRUE,
  );

  return system_settings_form($form);
}

/**
 * Otp settings form.
 *
 * @see sns_otp_login_menu().
 */
function sns_otp_login_config($form, &$form_state) {
  $roles = user_roles();
  $form['sns_otp_roles'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Roles'),
    '#options' => $roles,
    '#multiple' => TRUE,
    '#default_value' => variable_get('sns_otp_roles', array()),
    '#description' => t('Select roles on which you want OTP login functionality.'),
  );
  $form['sns_otp_aws_sms_type'] = array(
    '#type' => 'select',
    '#title' => t('Sms Type'),
    '#options' => array(
      'Transactional' => t('Transactional'),
      'Promotional' => t('Promotional'),
     ),
    '#multiple' => FALSE,
    '#default_value' => variable_get('sns_otp_aws_sms_type', 'Transactional'),
    '#description' => t('Select SMS type transactional has higher priority than promotional.'),
    '#required' => TRUE,
  );
  $fields = array_keys((array)field_info_instances('user')['user']);
  $fields = array_combine($fields, $fields);
  $form['sns_otp_mobile'] = array(
    '#type' => 'select',
    '#title' => t('Mobile'),
    '#options' => $fields,
    '#multiple' => FALSE,
    '#default_value' => variable_get('sns_otp_mobile', ''),
    '#description' => t('Select  field that contains Mobaile  number.'),
    '#required' => TRUE,
  );
  $form['sns_otp_timeout'] = array(
    '#type' => 'select',
    '#title' => t('Timeout'),
    '#options' => array(
      '.5' => t('30 seconds'),
      '1' => t('1 Minute'),
      '2' => t('2 Minute'),
      '3' => t('3 Minute'),
      '4' => t('4 Minute'),
      '5' => t('5 Minute'),
      '10' => t('10 Minute'),
      '15' => t('15 Minute'),
      '20' => t('20 Minute'),
      '25' => t('25 Minute'),
      '30' => t('30 Minute'),
      '60' => t('1 Hour'),
      'unlimited' => t('Unlimited'),
     ),
    '#multiple' => FALSE,
    '#default_value' => variable_get('sns_otp_timeout', 'unlimited'),
    '#description' => t("OTP life span"),
    '#required' => TRUE,
  );
  $form['sns_otp_sender_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Message Title'),
    '#default_value' => variable_get('sns_otp_sender_id', ''),
    '#description' => t("This depends on region you have selected for sns services if the region is sendr id enabled it will send Title."),
    '#required' => TRUE,
  );
  $form['sns_otp_message'] = array(
    '#type' => 'textarea',
    '#title' => t('OTP Message'),
    '#default_value' => variable_get('sns_otp_message', 'Your OTP is [user:login:otp].'),
    '#description' => check_plain(t('Enter OTP message use [user:login:otp] wherever you want to put OTP.')),
    '#required' => TRUE,
  );
  $form['sns_otp_allowed_attempts'] = array(
    '#type' => 'select',
    '#title' => t('Attempts'),
    '#options' => array(
      '0' => t('Unlimited'),
      '1' => t('1'),
      '3' => t('3'),
      '5' => t('5'),
      '10' => t('10'),
      '15' => t('15'),
     ),
    '#multiple' => FALSE,
    '#default_value' => variable_get('sns_otp_allowed_attempts', '0'),
    '#description' => t("Number of allowed OTP attempts"),
    '#required' => TRUE,
  );
  $form['sns_otp_block_time'] = array(
    '#type' => 'select',
    '#title' => t('Block Time'),
    '#options' => array(
      '0' => t('Do Not Block'),
      '1' => t('1 Minute'),
      '5' => t('5 Minute'),
      '10' => t('10 Minute'),
      '15' => t('15 Minute'),
      '20' => t('20 Minute'),
      '25' => t('25 Minute'),
      '30' => t('30 Minute'),
      '60' => t('1 Hour'),
      '120' => t('2 Hour'),
     ),
    '#multiple' => FALSE,
    '#default_value' => variable_get('sns_otp_block_time', '0'),
    '#description' => t("Bolck time after exhausting OTP attempt limit."),
    '#required' => TRUE,
  );

  return system_settings_form($form);
}
