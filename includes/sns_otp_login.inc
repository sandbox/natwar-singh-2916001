<?php
/**
 * @file
 * sns_otp_login form file.
 */

/**
 * OTP form.
 *
 * @param $form
 *   Form Array
 * @param &$form_state
 *   Form Sate
 * @param $uid
 *   User Id
 *
 * @return array
 *   Form Array
 */
function sns_otp_form($form, &$form_state, $uid = NULL) {
  $result = db_query("SELECT timestamp FROM sns_otp where uid = $uid AND session = '" . $_SESSION["session"] . "'")->fetchCol();
  $timestamp = isset($result[0]) ? $result[0] : NULL ;
  $timeout = variable_get('sns_otp_timeout', 'unlimited');
  if ($timeout != 'unlimited' && !empty($timestamp)) {
    $remain_time = $timeout - (REQUEST_TIME - $timestamp) / 60;
    if (!empty($remain_time) && !empty($timestamp)) {
      drupal_add_js(array('otp' => array('time' => $remain_time)), array('type' => 'setting'));
      drupal_add_js(drupal_get_path('module', 'sns_otp_login') . '/js/countdown.js');
    }
  }

  $form['otp'] = array(
    '#type' => 'password',
    '#title' => t('OTP'),
    '#title_display' => 'invisible',
    '#maxlength' => 6,
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Varify')
  );
  $form['resend'] = array(
    '#type' => 'submit',
    '#value' => t('Resend')
  );
  $session = session_id();
  $form_state['storage']['uid'] = $uid;

  return $form;
}

/**
 * OTP form validator.
 *
 * @param $form
 *   Form Array
 * @param &$form_state
 *   Form State
 */
function sns_otp_form_validate($form, &$form_state) {
  if ($form_state['triggering_element']['#value'] == 'Varify') {
    if (empty($form_state['input']['otp']) || !is_numeric($form_state['input']['otp'])) {
      form_set_error('otp', 'Please enter a valid otp.');
    }
  }
}

/**
 * OTP form submit handler.
 *
 * @param $form
 *   Form Array
 * @param &$form_state
 *   Form Sate
 */
function sns_otp_form_submit($form, &$form_state) {
  $uid = $form_state['storage']['uid'];
  if ($form_state['triggering_element']['#value'] == 'Resend') {
    // Clear flood event before OTP resend.
    flood_clear_event('sns_otp_login');
    $user = user_load($uid);
    $hash = get_account_hash($user);
    send_sns_otp($uid, $hash);
  }
  $result = db_query("SELECT uid, timestamp FROM sns_otp where uid = $uid AND session = '" . $_SESSION["session"] . "' And otp = " . $form_state['input']['otp'])->fetchAll();
  if(isset( $result[0] )) {
    $timeout = variable_get('sns_otp_timeout', 'unlimited');
    if ($timeout != 'unlimited') {
      $diff = (REQUEST_TIME - $result[0]->timestamp)/60;
      if ($diff > $timeout) {
        form_set_error('otp', 'Your OTP has been expired');
        return;
      }
    }
    otp_login($uid);
  }
  else {
    $window = variable_get('sns_otp_block_time', '0')*60;
    flood_register_event('sns_otp_login', $window);
    form_set_error('otp', 'Wrong OTP!');
  }
}

/**
 * Login through OTP.
 *
 * @param $uid
 *   User Id
 */
function otp_login($uid) {
  global $user;
  $user = user_load($uid);
  $form_state['redirect'] = '<front>';
  // Clear flood event.
  flood_clear_event('sns_otp_login');
  user_login_finalize($form_state);
}
