/*
 * @file implementation of timer
 */
(function($) {
  flag = 0;
  var CCOUNT
  var tout, count;
  Drupal.behaviors.question_paper = {
    attach: function (context, settings) {
        $('#edit-resend', context).prop('disabled', true);
        /*
         * Function to conver minuts to hh:mm:ss
         */
        function secondsTimeSpanToHMS(s) {
          var h = Math.floor(s/3600); //Get whole hours
          s -= h*3600;
          var m = Math.floor(s/60); //Get remaining minutes
          s -= m*60;
          //s = Math.round(s);
          return h+":"+(m < 10 ? '0'+m : m)+":"+(s < 10 ? '0'+s : s);
        }

      function cddisplay(context) {
        var hms=secondsTimeSpanToHMS(count)
        $('#edit-resend', context).attr('value', 'Resend in ' + hms);
        CCOUNT = count;
      }

      /*
       * Function to count down and fire ajax request on timeout
       */
      function countdown(context) {
        // starts countdown
        if (count <= 0) {
          clearInterval(tout);
          $('#edit-resend', context).attr('value', 'Resend');
          $('#edit-resend', context).prop('disabled', false);
          $('#edit-otp', context).prop('disabled', true);
          $('#edit-submit', context).css("display", "none");
        }
        else {
          count--;
          cddisplay(context);
        }
      }
      count = Math.round(Drupal.settings.otp.time*60);
      if (count <= 0) {
          $('#edit-resend', context).prop('disabled', false);
          $('#edit-otp', context).prop('disabled', true);
          $('#edit-submit', context).css("display", "none");
        }
      else {
      clearInterval(tout);
      cddisplay(context);
      tout = setInterval(countdown, 1000);
      }
    }
};})(jQuery);
